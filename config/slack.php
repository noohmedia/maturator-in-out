<?php

return [
    'incoming-webhook' => 'https://hooks.slack.com/services/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',

    'default_username' => 'Maturator',

    'default_emoji' => ':ghost:',
];
