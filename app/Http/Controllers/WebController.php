<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Code;
use App\Log;

class WebController extends Controller
{
    public function index(){
    	 
    }
    
    public function store(Request $request){
        $codes = collect(explode("\n", $request->input('codes')));
        $codeGroup = $codes->chunk(32)->toArray();
        
        if ($request->input('stock')){
          foreach ($codeGroup as $key => $value) {
              foreach ($value as $code){
                      Code::create([
                          'stock' => $request->input('stock'),
                          'value' => $request->input('value'),
                          'code' => $code,
                          ]);
              }
          }
        exit();
        }

        foreach ($codeGroup as $key => $value) {
            foreach ($value as $code){
                    Code::create([
                        'stock' => $key,
                        'value' => $request->input('value'),
                        'code' => $code,
                        ]);
            }
        }
    }    
    
    public function view(Request $request){
        
        $dateStart = $request->input('start');
        $dateEnd = $request->input('end');
        
        if ($dateStart) {
           $logs = Log::where('created_at', '>', $dateStart)
                      ->where('created_at', '<', $dateEnd)
                      ->orderBy('code')
                      ->get();
           
           $codes = Code::where('used_at', '>', $dateStart)
                         ->where('used_at', '<', $dateEnd)
                         ->orderBy('code')
                         ->get();



           return view('view', compact('logs','codes', 'dateStart', 'dateEnd'));
        } 


        return view('select');
    }
}
