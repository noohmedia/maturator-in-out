<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\GetGateways::class,
        Commands\GetSims::class,
        Commands\GenerateACD::class,
        Commands\MoveToPool::class,
        Commands\UpdateGsmForSims::class,
        Commands\PrepareToCall::class,
        Commands\Call::class,
        
        #Commands\Recharge::class,
        #Commands\Checker::class,
        /*check credit*/
        #Commands\CheckCredit::class,
        #Commands\CallCredit::class,
        /*acd*/
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
       $schedule->command('simserver:gateways')
                 ->withoutOverlapping()
                 ->everyMinute()
                 ->between('7:00', '22:00')
                 ->timezone('Europe/Bucharest');
        
        $schedule->command('simserver:simupdate')
                 ->withoutOverlapping()
                 ->everyMinute()
                 ->between('7:00', '22:00')
                 ->timezone('Europe/Bucharest');

/*        $schedule->command('simserver:acd')
                 ->everyMinute();*/
                 
        $schedule->command('prepare')
                 ->withoutOverlapping()
                 ->everyMinute()
                 ->between('7:00', '22:00')
                 ->timezone('Europe/Bucharest');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
