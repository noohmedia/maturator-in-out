<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Gateway;
use App\Sim;
use App\Port;
use Carbon\Carbon;


class UpdateGsmForSims extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simserver:simupdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sims = Sim::whereRaw('current_sim_pool = move_sim_pool')->get();
        if (count($sims) == 0){
            exit();
        }
        foreach ($sims as $key => $sim) {
            $sim->status = 0;
            $sim->gateway_card = null;
            $sim->gateway_port = null;
            $sim->sending_port = null;
            $sim->save();
           
            $simUpdate = $this->update($sim->icc);
            if (!empty($simUpdate['0'])){
                $gateway = Gateway::where('name', $simUpdate['0']->name)->first();

                $port = Port::where(['size' => $gateway->size, 
                                     'card' => $simUpdate['0']->card, 
                                     'port' => $simUpdate['0']->port
                                     ])->first();
                $sim->gateway_card = $simUpdate['0']->card; 
                $sim->gateway_port = $simUpdate['0']->port;
                $sim->sending_port = $port->gateway_port;
                $sim->gpool_name = $simUpdate['0']->name;
                //$sim->id_in_simserver = $simUpdate['0']->name;
                $sim->status = $simUpdate['0']->status;
                $sim->save();
            }
        }
    }

    private function update($icc) {
        $query = "SELECT * FROM gsmbox_view WHERE sim_id = '".$icc."'";
        return collect(DB::connection('pgsql')->select($query));
    }
}
