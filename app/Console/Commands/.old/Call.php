<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Sim;
use App\Log;
use App\Code;
use Carbon\Carbon;

use \phpseclib\Net\SSH2 as SSH2;
use \phpseclib\Crypt\RSA as RSA;

class Call extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'call {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call me maybe';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $simId = $this->argument('id');
        //$number = $this->argument('number');
        $output = "";
        $retryOutput = '';

        $sim = Sim::find($simId);
        
        $output .= "------------------------------------------------------------\n";
        echo $output .=  $sim->gateway->ip."\n";
        //todo: extract this to a method
        $ssh = new SSH2($sim->gateway->ip, 2222);
        $key = new RSA();
        $key->loadKey(file_get_contents(storage_path('key')));
        if (!$ssh->login('root', 'costin02')) {
            exit('Login Failed');
        }
        
        echo $output .=  $ssh->read('/(.*?)/');
        echo $output .=  $ssh->write("telnet localhost\n");
        echo $output .=  $ssh->read('/(.*?)/');
        echo $output .=  $ssh->write('res moni'."\n");
        echo $output .=  $ssh->read('/(.*?)/');
        $ssh->setTimeout(2);
        $output .= $ssh->write('set moni '.$sim->sending_port."\n");
        $output .= $ssh->setTimeout(2);
        echo $isMoniSet = $ssh->read('/(.*?)/');
        $output .= $isMoniSet;
        /*
         * Aici verificam daca s-a alocat portul
         */
        $isMoniSet = explode('set moni ', $isMoniSet);
        if (count($isMoniSet) < 2){
            $sim->recharging = 0;
            $sim->try = $sim->try+1;
            $sim->save();
            exit();
        }

        $ssh->setTimeout(5);

        /*
         * Aici luam un cod random
         */

        $code = Code::where('value', 4)
                    ->where('status', 'unsure')
                    ->inRandomOrder()
                    ->first();
        if (!$code){
        $code = Code::where('used_at', null)
             ->where('value', 4)
             ->where('status', null)
             ->inRandomOrder()
             ->first();
        }

        if(!$code){
            
            $this->error('nu mai avem coduri utilizabile boss');
            $sim->recharging = false;
            $sim->save();
            exit();
        }

        $code->status = "in use";
        $code->save();
        
        /*
         * Aici incercam sa reincarcam cu conexiunea ssh si codul
         */
        $output .= $this->makeRecharge($ssh, $code->code);

        /*
         * Verificam outputul, daca nu e ok, incercam cu alt cod
         */
        $status = $this->checker($output);
        if ($status == 'ok'){
            $code->used_at = Carbon::now(); 
            $code->status = $status;
            $code->save();
        }

        if ($status == 'wrong' || $status == 'used'){
            /*
             * Daca codul precedent nu a fost bun, il marcam cam nebun si folosi altul
             */
            $code->used_at = Carbon::now(); 
            $code->status = $status;
            $code->save();

            $sim->recharging = 0;
            $sim->try = $sim->try+1;
            $sim->save();
        }

        $creditOutput = '';
        $credit = '';
        if ($status == 'unknown'){
            
            $code->status = 'unsure';
            $code->save();
            $this->info('am ajuns la validare credit');
            $ssh->write('atd*133#'."\n");
            $creditOutput .= $ssh->read('/(.*?)/');
            $ssh->setTimeout(30);
            $creditOutput .= $ssh->read('/(.*?)/');
            $this->info($creditOutput);
            $credit = $this->checkCredit($creditOutput);
            
            if ($credit){
                $code->used_at = Carbon::now(); 
                $code->status = 'ok';                    
            } else {
                $code->used_at = Carbon::now(); 
                $code->status = 'unsure';
            }
            $code->save();
        }

        echo $output .=  $ssh->write('res moni'."\n");
        echo $output .=  $ssh->read('/(.*?)/');
        echo $output .=   "\nTimeout: ".$ssh->isTimeout()."";

        $this->info($output);

        $code->used_at = Carbon::now(); 
        $code->save();

        $log = Log::create([
            'icc' => $sim->icc,
            'try' => $sim->try,
            
            'response' => $output,
            'code' => $code->code,
            
            'creditresponse' => $creditOutput,
            
            'checkcredit' => $credit,
            
            'gateway_name' => $sim->gateway->name,
            'gateway_port' => $sim->sending_port,
        ]);

        if ($status == 'ok'){
            Sim::where('icc', $sim->icc)->delete();
        }

        if ($sim->try == 3 ){
            Sim::where('icc', $sim->icc)->delete();
        }
        
        echo $output .= "------------------------------------------------------------\n\n";

    }

    private function makeRecharge($ssh, $code){
            $output = '';
            $this->info('Using code: '.$code."\n");
            $output .= $ssh->read('/(.*?)/');
            $output .= $ssh->write('atd222'.$code.';'."\n");
            $output .= $ssh->read('/(.*?)/');
            $output .= $ssh->setTimeout(30);
            $output .= $ssh->read('/(.*?)/');
            return $output;
    }


    private function checker($response){

        if (strpos($response, 'centi s-au alocat in contul tau') !== false) {
            return 'ok';
        } 

        if (strpos($response, 'deja utilizat') !== false) {
            return 'used';
        }

        if (strpos($response, 'tastat gresit') !== false) {
            return 'wrong';
        }

        return 'unknown';
    }


    private function checkCredit($response){
        preg_match('/Ai\s([0-9,.\s]+)\sEuro/i', $response, $matches);
        if ($matches){
            return $matches['1'];
        }
    }

}
