<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


use App\Log;
use App\Sim;

class Checker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check {icc}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $icc = $this->argument('icc');

        $log = Log::where('icc', $icc)
                  ->orderBy('created_at', 'desc')
                  ->first();
        
        /* what is the status */
        $status = $this->checker($log->response);
        
        $log->status = $status;
        $log->credit =  $this->simCredit($log->response);
        $log->expires = $this->simExpires($log->response);
        $log->save();
        
        /*notify simserver*/
        if($status == "ok"){
            $this->simserver($status, $icc);
        }

        if ($log->try == 3){
            $this->simserver($status, $icc);
        }

    }

    private function simExpires($response){
        preg_match('/([0-9]{2}(-|\/)[0-9]{2}(-|\/)[0-9]{4})/i', $response, $matches);
        if ($matches){
            return $matches['0'];
        }
        return false;
    }
    
    private function simCredit($response){
        preg_match('/credit:\s([0-9,]+)centi/i', $response, $matches);
        if ($matches){
            return $matches['1'];
        }
        return false;
    }

    private function simserver($status, $icc){
        if ($status == 'ok'){
            $updateQuery = "update sims SET force_unload = 'TRUE', force_block = 'TRUE' where icc = '" . $icc . "'"; 
        }
        else {
            $updateQuery = "update sims SET force_unload = 'TRUE', force_block = 'TRUE', credit_block = 'TRUE' where icc = '".$icc."'";
        }

        return collect(\DB::connection('pgsql')->update($updateQuery));
        
    }


    private function checker($response){

        if (strpos($response, 'centi s-au alocat in contul tau') !== false) {
            return 'ok';
        } 

        if (strpos($response, 'deja utilizat') !== false) {
            return 'used';
        }

        if (strpos($response, 'tastat gresit') !== false) {
            return 'wrong';
        }

        return 'unknown';
    }


}
