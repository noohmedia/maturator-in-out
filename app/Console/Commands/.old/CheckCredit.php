<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use \App\Sim;
use \App\Log;
use \App\Code;
use Carbon\Carbon;


class CheckCredit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'credit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $sim = Sim::where('gpool_name', 'LIKE', 'YCNS%')
                   ->where('recharging', 0)
                   ->with('gateway')
                   ->inRandomOrder()
                   ->first();
        if (!$sim){
            exit();
        }
        
        $sim->recharging = true;
        $sim->save();
        

        \Artisan::call('callcredit', [
                        'id' => $sim->id
                       ]);

        $output = \Artisan::output();

        \Storage::disk('local')->put('/log/CHECKCREDIT-'.$sim->icc.'.txt',  $output);

    }
}
