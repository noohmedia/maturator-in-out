<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Sim;
use App\Log;
use App\Code;
use Carbon\Carbon;

use \phpseclib\Net\SSH2 as SSH2;
use \phpseclib\Crypt\RSA as RSA;

class CallCredit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'callcredit {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $simId = $this->argument('id');
        //$number = $this->argument('number');
        $output = "";
        $retryOutput = '';

        $sim = Sim::find($simId);
        
        $output .= "------------------------------------------------------------\n";
        echo $output .=  $sim->gateway->ip."\n";
        //todo: extract this to a method
        $ssh = new SSH2($sim->gateway->ip, 2222);
        $key = new RSA();
        $key->loadKey(file_get_contents(storage_path('key')));
        if (!$ssh->login('root', 'costin02')) {
            exit('Login Failed');
        }
        
        echo $output .=  $ssh->read('/(.*?)/');
        echo $output .=  $ssh->write("telnet localhost\n");
        echo $output .=  $ssh->read('/(.*?)/');
        echo $output .=  $ssh->write('res moni'."\n");
        echo $output .=  $ssh->read('/(.*?)/');
        $ssh->setTimeout(2);
        $output .= $ssh->write('set moni '.$sim->sending_port."\n");
        $output .= $ssh->setTimeout(2);
        echo $isMoniSet = $ssh->read('/(.*?)/');
        $output .= $isMoniSet;
        /*
         * Aici verificam daca s-a alocat portul
         */
        $isMoniSet = explode('set moni ', $isMoniSet);
        if (count($isMoniSet) < 2){
            $sim->recharging = 0;
            $sim->try = $sim->try+1;
            $sim->save();
            exit();
        }

        $ssh->setTimeout(5);
        
        /*
         * Aici incercam sa verificam creditul
         */
        $output .= $this->checkCredit($ssh);

        /*
         * Verificam outputul, daca nu e ok, incercam cu alt cod
         */

        echo $output .=  $ssh->write('res moni'."\n");
        echo $output .=  $ssh->read('/(.*?)/');
        echo $output .=   "\nTimeout: ".$ssh->isTimeout()."";
        $this->info($output);

        $status = $this->checker($output);


        if ($status == 'blocked'){
            $updateQuery = "update sims SET force_unload = 'TRUE', op_block = 'TRUE' where icc = '" . $sim->icc . "'";
            \DB::connection('pgsql')->update($updateQuery);
        }
        else {
            $updateQuery = "update sims SET force_unload = 'TRUE', force_block = 'TRUE' where icc = '" . $sim->icc . "'";
            \DB::connection('pgsql')->update($updateQuery);
        }    
        
        Sim::where('icc', $sim->icc)->delete();

    }


    private function checker($response){

        if (strpos($response, 'blocat') !== false) {
            return 'blocked';
        } 

        return 'ok';
    }


    private function checkCredit($ssh){
            $output = '';
            $output .= $ssh->read('/(.*?)/');
            $output .= $ssh->write('atd*133#'."\n");
            $output .= $ssh->read('/(.*?)/');
            $output .= $ssh->setTimeout(30);
            $output .= $ssh->read('/(.*?)/');
            return $output;
    }


}
