<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use \App\Sim;
use \App\Log;
use \App\Code;
use Carbon\Carbon;

class Recharge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simserver:recharge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if (!is_file(storage_path('_current_output.txt'))){
            file_put_contents(storage_path('_current_output.txt'), 0);
        }
        $counter =  (int) file_get_contents(storage_path('_current_output.txt'));
        if ($counter >= 16){
            $counter = 0;
        }
        $counter++;
        file_put_contents(storage_path('_current_output.txt'), $counter);
        sleep($counter);

        

        //sleep(rand(5,10));
        $sim = Sim::where('gpool_name', 'LIKE', 'INC_%')
                   ->where('recharging', 0)
                   ->with('gateway')
                   ->inRandomOrder()
                   ->first();
        if (!$sim){
            exit();
        }
        
        $now = Carbon::now();
        $loadTime = Carbon::parse($sim->last_load);
        $whenSimWasLoaded = $now->diffInMinutes($loadTime);
        if ($whenSimWasLoaded < 5){
            exit();
        }

        $sim->recharging = true;
        $sim->save();
        


        $query = "select last_unload from sims where icc = '".$sim->icc."'";
        $simUnloadTime = \DB::connection('pgsql')->select($query);
        $unload = $simUnloadTime['0']->last_unload;
        $now = Carbon::now();
        $unloadTime = Carbon::parse($unload);
        $whenSimWasUnloaded = $now->diffInMinutes($unloadTime);

        if ($whenSimWasUnloaded < 10){
            $log = Log::create([
                'icc' => $sim->icc,
                'response' => 'era in sleep boss',
                'code' => 'nici unu',
                'gateway_name' => $sim->gateway->name,
                'gateway_port' => $sim->sending_port,
            ]);
            Sim::where('icc', $sim->icc)->delete();
            exit();
        }



        \Artisan::call('call', [
                        'id' => $sim->id
                       ]);

        $output = \Artisan::output();

        \Storage::disk('local')->put('/log/'.$sim->icc.'.txt',  $output);


        \Artisan::call('check', [
                        'icc' => $sim->icc
                       ]);

    }
}
