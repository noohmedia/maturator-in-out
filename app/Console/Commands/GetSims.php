<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Gateway;
use App\Sim;
use App\Port;
use Carbon\Carbon;

class GetSims extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'simserver:getsims';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Get all the SIMs assigned to the Gateways';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
			parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{

		$allSims = $this->getAllSims();
		foreach ($allSims as $key => $sim) {
			$sendingPort = null;
			
			if ($sim->gid){
				$gateway = Gateway::where('id_in_simserver', $sim->gid)->first();

				$port = Port::where(['size' => $gateway->size, 
									 'card' => $sim->gcard, 
									 'port' => $sim->gport
									 ])->first();
				$sendingPort = $port->gateway_port;
			}
			$this->info($sim->icc);
			$simDetails = $this->getValidSims($sim->icc);
			if (
				
				$simDetails['0']->monitor_acd == 'caller' || 
				$simDetails['0']->monitor_acd == 'caller_16' || 
				$simDetails['0']->monitor_acd == 'receiver_1' ||
				$simDetails['0']->monitor_acd == 'receiver_2' || 
				$simDetails['0']->monitor_acd == 'receiver_3' ||
				$simDetails['0']->monitor_acd == 'receiver_only_1' ||
				$simDetails['0']->monitor_acd == 'receiver_only_2' || 
				$simDetails['0']->monitor_acd == 'receiver_only_3' 

				){
				

				if ($simDetails['0']->force_block == false){
					$moveToPool = $this->getMoveToPool($sim->icc);
					if ($simDetails['0']->monitor_acd == 'receiver_1' ||
						$simDetails['0']->monitor_acd == 'receiver_2' || 
						$simDetails['0']->monitor_acd == 'receiver_3' ||
						$simDetails['0']->monitor_acd == 'receiver_only_1' ||
						$simDetails['0']->monitor_acd == 'receiver_only_2' || 
						$simDetails['0']->monitor_acd == 'receiver_only_3' ){
						$callPool = $moveToPool['0']->id_call_in;
					}
					else {
						$callPool = $moveToPool['0']->id_call_out;
					}

					Sim::updateOrCreate(
						['icc' => $sim->icc],
						['id_in_simserver' => $sim->sid, 
						 'status' => $sim->status,
						 'type' => $simDetails['0']->monitor_acd,
						 'phone_no' => $sim->phone_no,
						 'move_sim_pool' => $callPool,
						 'gateway_card' => $sim->gcard,
						 'gateway_port' => $sim->gport,
						 'simserver_card' => $sim->scard,
						 'simserver_port' => $sim->sport,
						 'sending_port' => $sendingPort,
						 'gsm_operator' => $sim->goperator,
						 'gpool_id' => $sim->gpoolid,
						 'gpool_name' => $sim->gname,
						 'last_load' => $sim->last_load,
					]);	
				}
			}
		}
	\Artisan::call('simserver:acd');
	}


	private function getAllSims(){
		$query = "SELECT * from spools_view where icc is not null";
		return collect(DB::connection('pgsql')->select($query));
	}
	
	private function getValidSims($icc){
		$query = "SELECT monitor_acd,force_block from sims where icc = '".$icc."'";
		return collect(DB::connection('pgsql')->select($query));
	}
	
	private function getMoveToPool($icc){
		$query = "SELECT id_call_in,id_call_out from sims where icc = '".$icc."'";
		return collect(DB::connection('pgsql')->select($query));
	}

}