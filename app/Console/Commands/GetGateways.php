<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use \App\Gateway;
use \App\Sim;
use DB;
class GetGateways extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simserver:gateways';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Gateways from SIMserver';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $allSims = Sim::all();

        if (count($allSims) > 0){
            exit();
        }

        /*yolo*/ 
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        \App\Gateway::truncate();
        \App\Sim::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        $gateways = collect(DB::connection('pgsql')->select('select * from clients'));
        

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        foreach ($gateways as $gateway) {
            Gateway::updateOrCreate(
                ['name' => $gateway->name],
                ['ip' => $gateway->ip_reported,
                'id_in_simserver' => $gateway->id, 
                'name' => $gateway->name, 
                 'size' => $gateway->gsm_modules,
                 'type' => $gateway->ctype,
                 'status' => $gateway->status,
                ]
            );
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
       
        /*run the next one*/
        \Artisan::call('simserver:getsims');
    }
}
