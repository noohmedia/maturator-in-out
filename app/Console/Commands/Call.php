<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Sim;
use App\Log;
use App\Code;
use App\Gateway;
use Carbon\Carbon;

use \phpseclib\Net\SSH2 as SSH2;
use \phpseclib\Crypt\RSA as RSA;

class Call extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'call {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        

        $simId = $this->argument('id');
/*        var_dump('got: ', $simId);*/

        //$number = $this->argument('number');
        $output = "";
        $retryOutput = '';
        //\Artisan::call('simserver:simupdate');
        $sim = Sim::find($simId);
        
        $output .= "------------------------------------------------------------\n";
        //todo: extract this to a method
        $gateway = Gateway::where('name', $sim->gpool_name)->first();
        echo $output .=  $gateway->ip."\n";
        $ssh = new SSH2($gateway->ip, 2222);
        $key = new RSA();
        $key->loadKey(file_get_contents(storage_path('key')));
        if (!$ssh->login('root', 'costin02')) {
            $output .= $this->error('Login Failed');
        }
        
        echo $output .=  $ssh->read('/(.*?)/');
        echo $output .=  $ssh->write("telnet localhost\n");
        echo $output .=  $ssh->read('/(.*?)/');
        echo $output .=  $ssh->write('res moni'."\n");
        echo $output .=  $ssh->read('/(.*?)/');
        $ssh->setTimeout(2);
        $output .= $ssh->write('set moni '.$sim->sending_port."\n");
        $output .= $ssh->setTimeout(2);
        echo $isMoniSet = $ssh->read('/(.*?)/');
        $output .= $isMoniSet;
        /*
         * Aici verificam daca s-a alocat portul
         */
        $isMoniSet = explode('set moni ', $isMoniSet);
        if (count($isMoniSet) < 2){
            $sim->recharging = 0;
            $sim->try = $sim->try+1;
            $sim->save();
            exit();
        }

        $ssh->setTimeout(2);
        


        $randomNumber = $this->getReceiver();
        
/*        $calledCheck = Sim::where('phone_no', $randomNumber)->first();
        if ($calledCheck->type == 'receiver_1'){
            if ($calledCheck->received_calls == 1){
                $
            }
        } */

        if (!$randomNumber){
            echo $output .=  $ssh->write('res moni'."\n");
            echo $output .=  $ssh->read('/(.*?)/');
            echo $output .=  $ssh->write('quit'."\n");
            echo $output .=   "\nTimeout: ".$ssh->isTimeout()."";
            echo $output .=  'am ramas fara numere de sunat';
        }




        $output .= "calling: ". $randomNumber;
       
        $response = $this->caller($ssh, $randomNumber);
        $output .= $response;
        
        if( strpos( $response, 'BUSY' ) !== false ) {
           $output .= "NU RASPUNZI LA ESEMES";
           $output .= $ssh->write('ath'."\n");
           $now = \Carbon\Carbon::now();
           $output .= "\n\nEnded @ $now";

            $sim->failed_tries = $sim->failed_tries + 1;
            $sim->save();
            
            $today = \Carbon\Carbon::now();
            $time = "00:00:00";
            $caller = "Telnet";
            $called = $randomNumber;
            $sim_icc = $sim->icc;
            $gsmbox = $sim->gpool_name;
            $simPool = $sim->original_sim_pool_name;

            $nameQuery = "select name from sim_pools where id = ".$sim->current_sim_pool;
            
            $responseName = \DB::connection('pgsql')->select($nameQuery);

            $query = "INSERT INTO logs (time_id, message, durat, rdurat, charges, caller, called, sim_id, gsmbox_name, sim_pool) 
                           VALUES ('".$today."', 9999, '".$time."',  '".$time."', '0.00', '".$caller."', '".$called."', '".$sim_icc."', '".$gsmbox."', '".$responseName['0']->name."')";
            $queryResponse = \DB::connection('pgsql-prepaid')->select($query);


            \Artisan::call('call', [
                'id' => $sim->id
            ]);

        }

        if( strpos( $response, 'OK' ) !== false ) {
            $output .= "A RASPUNS?";
            $randomTime = rand(50, 95);
            $ssh->setTimeout($randomTime);
            $output .= $ssh->write('ath'."\n");
            $now = \Carbon\Carbon::now();
            $output .= "\n\nEnded @ $now";

            $today = \Carbon\Carbon::now();
    
            $time = $randomTime;
    
            $caller = "Telnet";
            $called = $randomNumber;
            $sim_icc = $sim->icc;
            $gsmbox = $sim->gpool_name;
            $simPool = $sim->original_sim_pool_name;
            
            $nameQuery = "select name from sim_pools where id = ".$sim->current_sim_pool;
			
			$responseName = \DB::connection('pgsql')->select($nameQuery);

            $query = "INSERT INTO logs (time_id, message, durat, rdurat, charges, caller, called, sim_id, gsmbox_name, sim_pool) 
                           VALUES ('".$today."', 9999, '".$time."',  '".$time."', '0.00', '".$caller."', '".$called."', '".$sim_icc."', '".$gsmbox."', '".$responseName['0']->name."')";
            $queryResponse = \DB::connection('pgsql-prepaid')->select($query);
            

            $updateQuery = "UPDATE sims SET total_callday = total_callday + 1, total_conn = total_conn + 1, total_call = total_call + 1 WHERE  icc = '" . $sim_icc . "'"; 
            \DB::connection('pgsql')->select($updateQuery);
            
            $sim->failed_tries = 0;
            
       
            $receiver = Sim::where('phone_no', $randomNumber)->first();
            $receiver->received_calls =  $receiver->received_calls + 1;
            $receiver->save();
            
            $updateQuery = "UPDATE sims SET intestcall_counter = intestcall_counter + 1 WHERE  icc = '" . $receiver->icc . "'"; 
            \DB::connection('pgsql')->select($updateQuery);

            


            $sim->calls_today = $sim->calls_today + 1;
            $sim->save();

        } else {
            $sim->failed_tries = $sim->failed_tries + 1;
            $sim->save();
        }

        /*
         * Verificam outputul, daca nu e ok, incercam cu alt cod
         */

        echo $output .=  $ssh->write('res moni'."\n");
        echo $output .=  $ssh->read('/(.*?)/');
        echo $output .=   "\nTimeout: ".$ssh->isTimeout()."";
        $this->info($output);


        //$status = $this->checker($output);

        

    }

    private function caller($ssh, $number){
            $output = '';
            $output .= $ssh->read('/(.*?)/');
            $now = \Carbon\Carbon::now();
            $output .= "\n\nCalling NOW!!! $now\n";
            $output .= $ssh->write('atd'.$number.'i;'."\n");
            $output .= $ssh->read('/(.*?)/');
            $output .= $ssh->setTimeout(rand(50, 95));
            $output .= $ssh->read('/(.*?)/');
        return $output;
    }


    private function getRandomNumberVoiceMail(){
        $query = "SELECT icc from sims where monitor_acd = 'call_in_vm' order by random() limit 20";
        $numberToCall = collect(\DB::connection('pgsql')->select($query));

        $randomIcc = $numberToCall->random();

        $query = "SELECT status, phone_no from spools_view where icc = '".$randomIcc->icc."'";
        $numberToCallStatus = collect(\DB::connection('pgsql')->select($query));


        if ($numberToCallStatus[0]->status > 20){
           $this->getRandomNumberVoiceMail();;
        }

        return $numberToCallStatus[0]->phone_no;

    }
    

    private function getReceiver(){
        $sims = Sim::where('type', 'LIKE', 'receiver%')->where('status', '>=', 30)->get();
        if ($sims->isEmpty()){
           return null;
        }
        return $sims->random()->phone_no;
    }




}
