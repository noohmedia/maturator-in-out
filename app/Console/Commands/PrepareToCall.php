<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Gateway;
use App\Sim;
use App\Port;
use Carbon\Carbon;

use \phpseclib\Net\SSH2 as SSH2;
use \phpseclib\Crypt\RSA as RSA;

class PrepareToCall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prepare';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Artisan::call('simserver:simupdate');
        
        $this->checkReceivers();

        $receiver = Sim::whereNotNull('sending_port')
           ->where('status', '>=', 30)
           ->where('type', 'like', 'receiver%')
           ->get();


        $sims = Sim::whereNotNull('sending_port')
                   ->where('status', '>=', 30)
                   ->where('type', 'like', 'caller%')
                   ->get();
        
        if ($receiver->isEmpty()){
             foreach ($sims as $key => $sim) {
                $this->moveEverythingBack($sim);

             }
            \Slack::send("[Maturator IN OUT] Nu mai sunt simuri de sunat, stergem tot!"); 
        }


        foreach ($sims as $key => $sim) {
            $this->info($sim->icc);


            //$timesToCall = 3;


            $check = Sim::where('icc', $sim->icc)->first();
            if ($check->calls_today < $check->max_calls_today){
                        $this->checkReceivers();
                        \Artisan::call('call', [
                            'id' => $sim->id
                        ]);
            } else {
                $this->moveEverythingBack($sim);
                $sim->delete();

            }

/*            $check = Sim::where('icc', $sim->icc)->first();
            if ($check->calls_today < $check->max_calls_today){
                        $this->checkReceivers();
                        \Artisan::call('call', [
                            'id' => $sim->id
                        ]);
            } else {
                $this->moveEverythingBack($sim);
                $sim->delete();
            }

            $check = Sim::where('icc', $sim->icc)->first();
            if ($check->calls_today < $check->max_calls_today){
                        $this->checkReceivers();
                        \Artisan::call('call', [
                            'id' => $sim->id
                        ]);
            } else {
                $this->moveEverythingBack($sim);
                $sim->delete();
            }

            $check = Sim::where('icc', $sim->icc)->first();
            if ($check->calls_today < $check->max_calls_today){
                        $this->checkReceivers();
                        \Artisan::call('call', [
                            'id' => $sim->id
                        ]);
            } else {
                $this->moveEverythingBack($sim);
                $sim->delete();
            }

            $check = Sim::where('icc', $sim->icc)->first();
            if ($check->calls_today < $check->max_calls_today){
                        $this->checkReceivers();
                        \Artisan::call('call', [
                            'id' => $sim->id
                        ]);
            } else {
                $this->moveEverythingBack($sim);
                $sim->delete();
            }

            $check = Sim::where('icc', $sim->icc)->first();
            if ($check->calls_today < $check->max_calls_today){
                        $this->checkReceivers();
                        \Artisan::call('call', [
                            'id' => $sim->id
                        ]);
            } else {
                $this->moveEverythingBack($sim);
                $sim->delete();
            }

            $check = Sim::where('icc', $sim->icc)->first();
            if ($check->calls_today < $check->max_calls_today){
                        $this->checkReceivers();
                        \Artisan::call('call', [
                            'id' => $sim->id
                        ]);
            } else {
                $this->moveEverythingBack($sim);
                $sim->delete();
            }

            $check = Sim::where('icc', $sim->icc)->first();
            if ($check->calls_today < $check->max_calls_today){
                        $this->checkReceivers();
                        \Artisan::call('call', [
                            'id' => $sim->id
                        ]);
            } else {
                $this->moveEverythingBack($sim);
                $sim->delete();
            }
                  */
          

            if ($sim->failed_tries == 0){
    
               if ($sim->calls_today == 4){
                $blockQuery = "UPDATE sims SET force_block = 'TRUE' where icc = '" . $sim->icc . "'"; 
                \DB::connection('pgsql')->select($blockQuery);
            	
            	$this->moveEverythingBack($sim);
            
            }
            
            
                
            }
        

            $output = \Artisan::output();
    
            \Storage::disk('local')->put('/log/MATURATOR/'.$sim->icc.'.txt', $output);

        }    

    }

    private function checkReceivers() {
        $receiver = Sim::whereNotNull('sending_port')
           ->where('status', '>=', 30)
           ->where('type', 'like', 'receiver%')
           ->get();
        
        foreach ($receiver as $key => $receiverSim) {
            if ($receiverSim->type == 'receiver_1' || $receiverSim->type == 'receiver_only_1'){
                $limit = 1;
            }

            if ($receiverSim->type == 'receiver_2' || $receiverSim->type == 'receiver_only_2'){
                $limit = 2; 
            }

            if ($receiverSim->type == 'receiver_3' || $receiverSim->type == 'receiver_only_3'){
                $limit = 3; 
            }
            

            if ($receiverSim->type != 'receiver_only_1' || $receiverSim->type != 'receiver_only_2' || $receiverSim->type != 'receiver_only_3'){
                if ($receiverSim->received_calls == $limit){
                    $removeQuery = "UPDATE sims SET monitor_acd = 'call_out_16' where icc = '" . $receiverSim->icc . "'"; 
                    \DB::connection('pgsql')->select($removeQuery);  
                    $this->moveEverythingBack($receiverSim);
                    \Slack::send("[Maturator IN OUT] Terminat de primit ".$limit." apeluri, pentru simul *".$receiverSim->icc."* se muta in Maturatoru v1 cu *call_out_16*"); 
                    $receiverSim->delete();

                }
            } else {
                $this->moveEverythingBack($receiverSim);
                \Slack::send("[Maturator IN OUT] Terminat de primit ".$limit." apeluri, pentru simul *".$receiverSim->icc."*"); 
                $receiverSim->delete();
            }
        }

    }


    private function moveEverythingBack($sim) {
        $unloadQuery = "UPDATE sims SET force_unload = 'TRUE' where icc = '" . $sim->icc . "'"; 
        \DB::connection('pgsql')->select($unloadQuery);

        $moveQueryNull = "UPDATE simbox SET pool_id = null WHERE sim_id = '".$sim->icc."'";
        \DB::connection('pgsql')->select($moveQueryNull); 

        $moveQuery = "UPDATE simbox SET pool_id = ".$sim->original_sim_pool." WHERE sim_id = '".$sim->icc."'";
        \DB::connection('pgsql')->select($moveQuery);
        
/* 
        if ($sim->type == 'caller_16'){

            $totalConnQuery = "SELECT total_conn from sims where icc = '" . $sim->icc . "'";
            $totalConnEesponse = \DB::connection('pgsql')->select($totalConnQuery);

            if ($totalConnEesponse['0']->total_conn >= 16){
                $removeQuery = "UPDATE sims SET monitor_acd = null, force_block = 'TRUE' where icc = '" . $sim->icc . "'"; 
                \DB::connection('pgsql')->select($removeQuery);  
            }
        
        }*/
        Sim::where('icc', $sim->icc)->delete();

        $remaining = Sim::all()->count();
        \Slack::send("[Maturator IN OUT]Terminat sim-ul *".$sim->icc."*, Rămase: ". $remaining); 

    }


}
