<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Gateway;
use App\Sim;
use App\Port;
use Carbon\Carbon;


class GenerateACD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simserver:acd';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $sims = Sim::all();
        foreach ($sims as $sim) {
            $this->info('doing: '. $sim->icc);

            $queryPoolId = "SELECT * from spools_view where icc = '".$sim->icc."'";
            $simPoolId = collect(DB::connection('pgsql')->select($queryPoolId));
            //dd($simPool);
            $queryPoolName = "SELECT * from sim_pools where id = '".$simPoolId['0']->pool_id."'";
            $simPool = collect(DB::connection('pgsql')->select($queryPoolName));
            

             
            if ($simPool){
                $sim->original_sim_pool_name = $simPool['0']->name;
                $sim->original_sim_pool = $simPool['0']->id;
               

                $sim->current_sim_pool = $simPool['0']->id;
                $sim->max_calls_today = $simPool['0']->max_callday;
                $sim->calls_today = $this->callsToday($sim->icc)[0]->total_callday;
                $sim->save(); 
            }

            $today = \Carbon\Carbon::today()->toDateString();
            $today = "$today 00:00:00";
            $query = "SELECT * FROM logs WHERE sim_id = '". $sim->icc ."' and caller = 'Telnet' and time_id >= '". $today ."'";

            $logs = collect(DB::connection('pgsql-prepaid')->select($query));
            $noOfCalls = $logs->count();

            if ($noOfCalls >= 11){
                Sim::find($sim->id)->delete();
            }

        }

    \Artisan::call('simserver:move');
    
    }

    private function callsToday($icc){
        $query = "select total_callday from sims where icc = '".$icc."'";
        return collect(DB::connection('pgsql')->select($query));
    }

}
