<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;
use App\Gateway;
use App\Sim;
use App\Port;
use Carbon\Carbon;

use \phpseclib\Net\SSH2 as SSH2;
use \phpseclib\Crypt\RSA as RSA;

class MoveToPool extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simserver:move';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        

        $sims = Sim::whereRaw('current_sim_pool != move_sim_pool')->get();


        foreach ($sims as $key => $sim) {
            //var_dump($sim->icc);
            $this->moveToOutPool($sim->icc, $sim->move_sim_pool);
        }
    }

    private function moveToOutPool($icc, $movePool) {
        Sim::where('icc', $icc)->update(['current_sim_pool' => $movePool]);
        
        $unloadQuery = "UPDATE sims SET force_unload = 'TRUE' WHERE icc = '" . $icc . "'"; 
        DB::connection('pgsql')->select($unloadQuery);
        // de facut cu NULL
        $moveQuery = "UPDATE simbox SET pool_id = ".$movePool." WHERE sim_id = '".$icc."'";
        DB::connection('pgsql')->select($moveQuery);

    }

}