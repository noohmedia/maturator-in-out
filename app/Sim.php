<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sim extends Model
{
    protected $guarded = [];

	public function gateway(){
		return $this->belongsTo('App\Gateway', 'id_in_simserver', 'id_in_simserver');
	}

}