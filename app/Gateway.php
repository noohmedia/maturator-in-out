<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gateway extends Model
{
	protected $guarded = [];

	public function sims(){
		return $this->hasMany('App\Sim', 'id_in_simserver', 'id_in_simserver');
	}





	public function getStatusAttribute($value)
	{
		return $this->getStatus($value);
	}

	


	public function getStatus($status = null)
	{
		switch ($status) {
			case '-1':
				return 'offline';
			case '0':
				return 'authenticating';
			case '10':
				return 'online';
		}
	}
}
