<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCallsTodayToSimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sims', function (Blueprint $table) {
            $table->integer('calls_today')->nullable()->after('acd');
            $table->integer('received_calls')->nullable()->after('calls_today');
            $table->integer('max_calls_today')->nullable()->after('received_calls');
            $table->integer('time_today')->nullable()->after('max_calls_today');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sims', function (Blueprint $table) {
            $table->dropColumn('calls_today');
            $table->dropColumn('received_calls');
            $table->dropColumn('max_calls_today');
            $table->dropColumn('time_today');
        });
    }
}
