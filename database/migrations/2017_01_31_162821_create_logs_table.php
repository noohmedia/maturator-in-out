<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('icc');
            $table->string('gateway_name');
            $table->string('gateway_port');

            $table->integer('try')->default(0);
            $table->string('status')->nullable();
            $table->string('credit')->nullable();
            $table->string('retrystatus')->nullable();
            $table->string('checkcredit')->nullable();
            
            $table->string('code')->nullable();
            $table->string('retrycode')->nullable();
            $table->string('expires')->nullable();
            
            $table->text('response')->nullable();
            $table->text('retryresponse')->nullable();
            $table->text('creditresponse')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
