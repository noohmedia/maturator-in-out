<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sims', function (Blueprint $table) {
            $table->increments('id');
            $table->string('icc');
            $table->char('sending_port', 3)->nullable();
            
            $table->integer('original_sim_pool')->nullable();
            $table->integer('move_sim_pool')->nullable();
            $table->integer('current_sim_pool')->nullable();

            $table->integer('status');
            $table->string('type');

            $table->integer('failed_tries')->default(0);
            $table->integer('in_use')->default(0);
            $table->integer('try')->default(0);
            //$table->boolean('recharging')->default(false);

            $table->string('last_load')->nullable();
            //$table->integer('recharge_with')->nullable();

            $table->integer('gateway_card')->nullable();
            $table->integer('gateway_port')->nullable();
            $table->integer('simserver_card')->nullable();
            $table->integer('simserver_port')->nullable();
            
            $table->integer('id_in_simserver')->nullable();
            
            $table->string('imei')->nullable();
            $table->string('imsi')->nullable();
            $table->string('gsm_operator')->nullable();
            $table->integer('gpool_id')->nullable();
            $table->string('gpool_name')->nullable();
            $table->string('original_sim_pool_name')->nullable();
            
            
            
            $table->foreign('id_in_simserver')
                  ->references('id_in_simserver')->on('gateways')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sims');
    }
}
