<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VoiBridgeSeeder::class);
        $this->call(VoxiPlusSeeder::class);
        $this->call(MultiAccess32Seeder::class);
        $this->call(MultiAccess64Seeder::class);
    }
}
