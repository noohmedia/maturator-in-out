<?php

use Illuminate\Database\Seeder;

class MultiAccess64Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
    	$entries = 
    	[
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '0', 'port' =>	'0', 'gateway_port' => '000'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '0', 'port' =>	'1', 'gateway_port' => '001'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '0', 'port' =>	'2', 'gateway_port' => '002'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '0', 'port' =>	'3', 'gateway_port' => '003'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '1', 'port' =>	'0', 'gateway_port' => '008'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '1', 'port' =>	'1', 'gateway_port' => '009'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '1', 'port' =>	'2', 'gateway_port' => '010'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '1', 'port' =>	'3', 'gateway_port' => '011'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '2', 'port' =>	'0', 'gateway_port' => '016'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '2', 'port' =>	'1', 'gateway_port' => '017'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '2', 'port' =>	'2', 'gateway_port' => '018'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '2', 'port' =>	'3', 'gateway_port' => '019'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '3', 'port' =>	'0', 'gateway_port' => '024'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '3', 'port' =>	'1', 'gateway_port' => '025'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '3', 'port' =>	'2', 'gateway_port' => '026'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '3', 'port' =>	'3', 'gateway_port' => '027'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '4', 'port' =>	'0', 'gateway_port' => '032'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '4', 'port' =>	'1', 'gateway_port' => '033'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '4', 'port' =>	'2', 'gateway_port' => '034'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '4', 'port' =>	'3', 'gateway_port' => '035'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '5', 'port' =>	'0', 'gateway_port' => '040'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '5', 'port' =>	'1', 'gateway_port' => '041'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '5', 'port' =>	'2', 'gateway_port' => '042'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '5', 'port' =>	'3', 'gateway_port' => '043'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '6', 'port' =>	'0', 'gateway_port' => '048'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '6', 'port' =>	'1', 'gateway_port' => '049'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '6', 'port' =>	'2', 'gateway_port' => '050'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '6', 'port' =>	'3', 'gateway_port' => '051'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '7', 'port' =>	'0', 'gateway_port' => '056'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '7', 'port' =>	'1', 'gateway_port' => '057'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '7', 'port' =>	'2', 'gateway_port' => '058'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '7', 'port' =>	'3', 'gateway_port' => '059'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '8', 'port' =>	'0', 'gateway_port' => '064'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '8', 'port' =>	'1', 'gateway_port' => '065'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '8', 'port' =>	'2', 'gateway_port' => '066'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '8', 'port' =>	'3', 'gateway_port' => '067'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '9', 'port' =>	'0', 'gateway_port' => '072'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '9', 'port' =>	'1', 'gateway_port' => '073'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '9', 'port' =>	'2', 'gateway_port' => '074'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '9', 'port' =>	'3', 'gateway_port' => '075'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '10', 'port' =>	'0', 'gateway_port' => '080'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '10', 'port' =>	'1', 'gateway_port' => '081'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '10', 'port' =>	'2', 'gateway_port' => '082'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '10', 'port' =>	'3', 'gateway_port' => '083'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '11', 'port' =>	'0', 'gateway_port' => '088'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '11', 'port' =>	'1', 'gateway_port' => '089'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '11', 'port' =>	'2', 'gateway_port' => '090'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '11', 'port' =>	'3', 'gateway_port' => '091'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '12', 'port' =>	'0', 'gateway_port' => '096'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '12', 'port' =>	'1', 'gateway_port' => '097'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '12', 'port' =>	'2', 'gateway_port' => '098'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '12', 'port' =>	'3', 'gateway_port' => '099'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '13', 'port' =>	'0', 'gateway_port' => '104'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '13', 'port' =>	'1', 'gateway_port' => '105'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '13', 'port' =>	'2', 'gateway_port' => '106'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '13', 'port' =>	'3', 'gateway_port' => '107'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '14', 'port' =>	'0', 'gateway_port' => '112'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '14', 'port' =>	'1', 'gateway_port' => '113'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '14', 'port' =>	'2', 'gateway_port' => '114'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '14', 'port' =>	'3', 'gateway_port' => '115'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '15', 'port' =>	'0', 'gateway_port' => '120'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '15', 'port' =>	'1', 'gateway_port' => '121'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '15', 'port' =>	'2', 'gateway_port' => '122'],	
			['name' => 'MultiAccess64', 'size' => '64', 'card' => '15', 'port' =>	'3', 'gateway_port' => '123'],	

		];
		
		foreach($entries as $entry){
          	DB::table('ports')->insert([
				'name' => $entry['name'],
				'size' => $entry['size'],
				'card' => $entry['card'],
				'port' => $entry['port'],
				'gateway_port' => $entry['gateway_port'],				
            ]);
    	}
   	}
}
