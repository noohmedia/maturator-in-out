<?php

use Illuminate\Database\Seeder;

class MultiAccess32Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$entries = 
    	[
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '0', 'port' => '0', 'gateway_port' => '000'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '0', 'port' => '1', 'gateway_port' => '001'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '1', 'port' => '0', 'gateway_port' => '008'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '1', 'port' => '1', 'gateway_port' => '009'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '2', 'port' => '0', 'gateway_port' => '016'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '2', 'port' => '1', 'gateway_port' => '017'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '3', 'port' => '0', 'gateway_port' => '024'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '3', 'port' => '1', 'gateway_port' => '025'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '4', 'port' => '0', 'gateway_port' => '032'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '4', 'port' => '1', 'gateway_port' => '033'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '5', 'port' => '0', 'gateway_port' => '040'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '5', 'port' => '1', 'gateway_port' => '041'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '6', 'port' => '0', 'gateway_port' => '048'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '6', 'port' => '1', 'gateway_port' => '049'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '7', 'port' => '0', 'gateway_port' => '056'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '7', 'port' => '1', 'gateway_port' => '057'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '8', 'port' => '0', 'gateway_port' => '064'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '8', 'port' => '1', 'gateway_port' => '065'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '9', 'port' => '0', 'gateway_port' => '072'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '9', 'port' => '1', 'gateway_port' => '073'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '10', 'port' => '0', 'gateway_port' => '080'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '10', 'port' => '1', 'gateway_port' => '081'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '11', 'port' => '0', 'gateway_port' => '088'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '11', 'port' => '1', 'gateway_port' => '089'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '12', 'port' => '0', 'gateway_port' => '096'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '12', 'port' => '1', 'gateway_port' => '097'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '13', 'port' => '0', 'gateway_port' => '104'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '13', 'port' => '1', 'gateway_port' => '105'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '14', 'port' => '0', 'gateway_port' => '112'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '14', 'port' => '1', 'gateway_port' => '113'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '15', 'port' => '0', 'gateway_port' => '120'],	
			['name' => 'MultiAccess32', 'size' => '32',  'card' => '15', 'port' => '1', 'gateway_port' => '121'],	

		];
		
		foreach($entries as $entry){
          	DB::table('ports')->insert([
				'name' => $entry['name'],
				'size' => $entry['size'],
				'card' => $entry['card'],
				'port' => $entry['port'],
				'gateway_port' => $entry['gateway_port'],				
            ]);
    	}
    }
}
