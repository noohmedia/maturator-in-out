<?php

use Illuminate\Database\Seeder;

class VoiBridgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $entries = [
				['name' => 'VoiBridge','size' => '4' ,'card' => '4', 'port' => '0',	'gateway_port' => '032'],
				['name' => 'VoiBridge','size' => '4' ,'card' => '4', 'port' => '1',	'gateway_port' => '033'],
				['name' => 'VoiBridge','size' => '4' ,'card' => '8', 'port' => '0',	'gateway_port' => '064'],
				['name' => 'VoiBridge','size' => '4' ,'card' => '8', 'port' => '1',	'gateway_port' => '065'],
			];
		

		foreach($entries as $entry){
          	DB::table('ports')->insert([
				'name' => $entry['name'],
				'size' => $entry['size'],
				'card' => $entry['card'],
				'port' => $entry['port'],
				'gateway_port' => $entry['gateway_port'],
            ]);
    	}
    }
}
