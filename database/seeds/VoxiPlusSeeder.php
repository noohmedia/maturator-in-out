<?php

use Illuminate\Database\Seeder;

class VoxiPlusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$entries = 
    	[
			['name' => 'VoxiPlus', 'size' => '12', 'card' => '0', 'port' => '1', 'gateway_port' => '001'],	
			['name' => 'VoxiPlus', 'size' => '12', 'card' => '0', 'port' => '2', 'gateway_port' => '002'],	
			['name' => 'VoxiPlus', 'size' => '12', 'card' => '0', 'port' => '3', 'gateway_port' => '003'],	
			['name' => 'VoxiPlus', 'size' => '12', 'card' => '1', 'port' => '0', 'gateway_port' => '008'],	
			['name' => 'VoxiPlus', 'size' => '12', 'card' => '1', 'port' => '1', 'gateway_port' => '009'],	
	    	['name' => 'VoxiPlus', 'size' => '12', 'card' => '0', 'port' => '0', 'gateway_port' => '000'],	
			['name' => 'VoxiPlus', 'size' => '12', 'card' => '1', 'port' => '2', 'gateway_port' => '010'],	
			['name' => 'VoxiPlus', 'size' => '12', 'card' => '1', 'port' => '3', 'gateway_port' => '011'],	
			['name' => 'VoxiPlus', 'size' => '12', 'card' => '2', 'port' => '0', 'gateway_port' => '016'],	
			['name' => 'VoxiPlus', 'size' => '12', 'card' => '2', 'port' => '1', 'gateway_port' => '017'],	
			['name' => 'VoxiPlus', 'size' => '12', 'card' => '2', 'port' => '2', 'gateway_port' => '018'],	
			['name' => 'VoxiPlus', 'size' => '12', 'card' => '2', 'port' => '3', 'gateway_port' => '019'],	
		];
		
		foreach($entries as $entry){
          	DB::table('ports')->insert([
				'name' => $entry['name'],
				'size' => $entry['size'],
				'card' => $entry['card'],
				'port' => $entry['port'],
				'gateway_port' => $entry['gateway_port'],				
            ]);
    	}
    }
}
