<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.min.css">
      
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js"></script>
     <script src="/js/bootstrap.min.js"></script>
    <script src="/js/scripts.js"></script>
  </head>
  <body>
</head>
<body>
 

    <div class="container-fluid">

<div class="row">
 
<form method="post" action="/view" class="form-inline">
  <div class="form-group">
    <label for="email">Start:</label>
    <input type="text" value="{{ $dateStart }}" name="start" id="datepicker">
  </div>
  <div class="form-group">
    <label for="pwd">End:</label>
    <input type="text" value="{{ $dateEnd }}" name="end" id="datepicker2">
  </div>

  <button type="submit" class="btn btn-success">BAGA!!!!</button>
</form>

  <script>
    $('#datepicker').datetimepicker({
/*      format:'H:i',
      value:'12:00'
*/    });
    $('#datepicker2').datetimepicker({
/*      format:'H:i',
      value:'12:00'
*/    });


  </script>

</div>

<hr>

    <div class="row">
        <div class="col-md-8">
            <table class="table table-hover table-bordered table-condensed">
                <thead>
                    <tr>
                        <th>
                            ICC
                        </th>
                        <th>
                            STATUS
                        </th>
                        <th>
                            COD
                        </th>
                        <th>
                            Modified
                        </th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($logs  as $log)

                    <tr class="active">
                        <td>
                          {{$log->icc}}
                        </td>
                        <td>
                          {{$log->status}}
                        </td>
                        <td>
                           {{$log->code}}
                        </td>
                        <td>
                          {{$log->created_at}}
                        </td>
                    </tr>

                    @endforeach

                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <table class="table table-condensed table-bordered table-hover">
                <thead>
                    <tr>
                        <th>
                            COD
                        </th>
                        <th>
                            Status
                        </th>
                        <th>
                            Folosit la
                        </th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($codes  as $code)

                    <tr class="active">
                        <td>
                          {{$code->code}}
                        </td>
                        <td>
                          {{$code->status}}
                        </td>
                        <td>
                          {{$code->used_at}}
                        </td>


                    </tr>

                    @endforeach
                   
                </tbody>
            </table>
        </div>
    </div>
</div>


  </body>
</html>